//console.log("Hello World");


/*let count = 5;*/

// While Loop
/*
	-takes in an expression/condition
	-expressions are any unit of code that can be evaluated to a value
	-if the evaluation of the condition is true, the statements will be executed.
	-a loop will iterate a certain number of times until the expression is/is not met.
	-"iteration" is the term given to the repetition of the statement/s

	SYNTAX:
		while (expression/condition){
			statement/s;
		}
*/

/*while (count !== 0) {
	console.log("While loop result: " + count);

	count --;
};*/

/*
	miniactivity:
		reassign a value to the count variable, make it 0
		create a while loop that checks the value of the count
			as long as the value is less than or equal to 10, log the console in the console
			increment the value of count
			if the value reaches 11 or higher, the loop will stop
*/

/*let cnt = 0
// checks for the value of count
while (cnt <=10){
	// the main statement to be performed if the expression remains true
	console.log("While loop result: "+ cnt);

	//changes the value of the count variable, serves as the loop breaker
	cnt++;
}*/

// Do-while loop
/*
	- a do-while loop works a lot like the while loop. but unlike while loop, do-while loops guarantee that the code will be executed at least once.

	SYNTAX:
	do{
		statement/s
	} while (expression/condition)
*/
/*
	- Number function works similarly to the "parseInt" function
	- both differ significantly in terms of the process they undertake in converting information into a number data type.
 	How do-while loop works:
 		1. the statement in 'do' block executes once
 		2. the message in the statements will be executed
 		3. after executing once, the while statement will evaluate whether to run the iteration of the loop or not based on the expression(if the number is less than 10)
 		4. if the condition is true, an iteration will be done.
 		5. if the condition is false, the loop will stop
*/

/*let number = Number(prompt("Give me a number "));
do{
	console.log("Do-while Loop: " + number);

	// number++ is the same
	number+=1;

}while (number<10);*/


// For Loop
/*
	- the most flexible looping compared to do-while and while loops. It has 3 parts.
	1. initialization - tracks the progression of the loop(varaible setting)
	2. condition/expression - will be evaluated which will determine whether the loop will run one more time.
	3. finalExpression - indicates how to advance the loop

	-Stages of For Loop
		- will initialize a variable "count" that has the value of 0
		- the condition/expression that is to be assessed if if the value of "count" is less than or equal to 20
		- perform the statements should the condition/expression returns true
		- increment the value of count
*/

for (let count=0; count<=20; count++){
	console.log("For Loop: " + count);
}

// using strings 
/*
	characters in a string may be counted using .length property. the property measures the number of elements (characters), not the string itself. It returns a number

	spaces and other special characters are also accounted in terms of counting the characters inside a string

	strings are special compared to other data types in that it has access to functions and other pieces of information another data type might not have
*/

let myString = "alex";
//console.log(myString);
console.log(myString.length);

/*
	the function below looks like we are performing the following. These are also examples on how we ca get access to the characters inside the string itself
		console.log(myString[0]);
		console.log(myString[1]);
		console.log(myString[2]);
		console.log(myString[3]);
*/

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

/*
	Miniactivity
		create a let variable "myName" that has your name as the value
		using loop, log in the console each character of the variable but make sure that the string will not be case sensitive
*/

let myName = "COEL";

for(let x = 0; x < myName.length; x++){
	if(
		myName[x].toLowerCase() == "a" ||
		myName[x].toLowerCase() == "e" ||
		myName[x].toLowerCase() == "i" ||
		myName[x].toLowerCase() == "o" ||
		myName[x].toLowerCase() == "u"
	){
		console.log(3);
	}else{
		console.log(myName[x].toLowerCase());
	}
}

// Continue and Break Statements
/*
	the "continue" allows the code to go to the next iteration of the loop without finishing the execution of all statements in the block

	the "break" is used to terminate the current loop once a match has been found/the condition returns true
*/

for(let count=0; count<=20; count++){
	if(count%2===0){
		// tells the code to continue tho the next iteration of the loop; it will ignore all preceeding of the block;
		continue;
	}
	// of the remainder is not equal to zero, this will be executed
	console.log("Continue and Break: " + count);

	if (count>10){
		// tells the code to terminate/stop the loop even if the expression /condition of loop will return true for the next iteration.
		break;
	};
};

let name = "alexandro";

/*

	miniactivity
		create a loop with continue and break statements
			try to log in the console each letter of the name and with a catch
				if the letter is a, continue to the next iteration
				if the letter is b, break the loop

		send the output in the google chat.
*/

for(let i=0; i<name.length; i++){
	console.log(name[i]); // will display "a and "d" in the string
	if(name[i].toLowerCase() === "a"){
		continue
	};
	// console.log(name[i]); - will not display all of the "a" in the string
	if(name[i].toLowerCase() == "d"){
			break;
	};
	//console.log(name[i]); - will not display all of the "a" and "d"in the string
};

/*for (let x=0;x<=3;x++){
	for(let y=0;y<=x;y++)
		console.log("x: " + x + "  y: " + y)
}*/