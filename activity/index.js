let num = prompt("Please give a number.")

console.log("The number you provided is " + num)

for(let i=num;i>0;i--){

	if(i<=50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

	if(i%10==0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	} else if (i%5==0){
		console.log(i);
	}
}

let word = "supercalifragilisticexpialidocious"
let wordArray = word.split('');
let newWordArray = []
console.log(word)

for(x=0;x<wordArray.length;x++){
	if( 
		wordArray[x] == "a" ||
		wordArray[x] == "e" ||
		wordArray[x] == "i" ||
		wordArray[x] == "o" ||
		wordArray[x] == "u"
	){
		continue
	}else{
		newWordArray.push(wordArray[x])
	}
}
console.log(newWordArray.join(''))